#!/bin/bash
set -euo pipefail

if [ ! $1 ]; then
        echo "Add package to build..."
	exit 1
else
	pkg=$1
	
	cd /package
	sudo chown -R builder .
	
	countries="Germany"
	
	if [ ! -z "${BRANCH:-}" ]; then
		pacman-mirrors -c"$countries" -a -B "$BRANCH"
	else
		pacman-mirrors -c"$countries"
	fi
	
	pacman --noconfirm --noprogressbar -Syyu
	
	echo "Building package..."
	sudo -u builder sudo chrootbuild -b unstable -cp $pkg

	exit 0
fi
