#!/bin/bash
DOCKER_USER=$1
DOCKER_ORGANIZATION=manjarolinux
DOCKER_IMAGE=build-testing

# deploy to docker hub
docker login -u ${DOCKER_USER}
docker push ${DOCKER_ORGANIZATION}/${DOCKER_IMAGE}
